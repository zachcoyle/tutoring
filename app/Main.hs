module Main where

import Lib (matchStudentsWithTutors)

-- 'optimized' in the context of the pairing in this program is:
-- finding the tutor that has the most overlap in subjects as the student
-- if more than one tutor ties, the first one in the list is chosen
-- I would choose a more rigorous method of tie-breaking in a real world scenario
--
-- improvements I would make if I had more time, would be
-- better paramaterization, making better use of the data structures in play
-- and load balancing the tutors more effectively
main :: IO ()
main = print matchStudentsWithTutors
