module Lib
  ( matchStudentsWithTutors
  ) where

import Data.List.Extras.Argmax (argmax)
import qualified Data.Map
import Data.Maybe

tutorEligibilities =
  Data.Map.fromList
    [ ("ACT Reading", ["Joakim"])
    , ("ACT Essay", ["Joakim", "Frieda"])
    , ("ACT Writing", ["Joakim", "Frieda"])
    , ("AP Chemistry", ["Bob"])
    , ("ACT Science", ["Bob", "Maggie"])
    , ("US History", ["Kseniya"])
    , ("ACT Math", ["Jamal"])
    ]

studentPreferences =
  [ ("Annie", ["ACT Math"])
  , ("Oskar", ["AP Chemistry"])
  , ( "Olle"
    , ["ACT Reading", "ACT Writing", "ACT Essay", "ACT Science", "ACT Math"])
  , ("Ingrid", ["US History", "AP Chemistry"])
  , ("Yuchen", ["ACT Math"])
  , ("Arjun", ["ACT Reading", "ACT Writing", "ACT Essay"])
  , ("Esmeralda", ["AP Chemistry"])
  ]

tutorsForSubject :: [String] -> [Maybe [String]]
tutorsForSubject = map (`Data.Map.lookup` tutorEligibilities)

-- had a little help from stack overflow for this function,
-- I'd intended to write it as a fold, but was running out of time
-- https://stackoverflow.com/a/34028745/6946424
mostFrequent :: Eq a => [a] -> a
mostFrequent xs = argmax f xs
  where
    f x = length $ filter (== x) xs

chooseTutor :: [Maybe [String]] -> String
chooseTutor = mostFrequent . concat . catMaybes

matchStudentsWithTutors = do
  student <- studentPreferences
  let studentName = fst student
  let studentSubjects = snd student
  let tutor = chooseTutor $ tutorsForSubject studentSubjects
  return (studentName, tutor)
